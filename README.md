All scripts used to produce the paper :

Zemp, D.C & Gérard, A. Hölscher, D, Ammer, C., Irawan, B., Sundawati, L., 
Teuscher, M., Kreft, H. Tree performance in a biodiversity enrichment experiment
in an oil-palm landscape, Journal of Applied Ecology, in press. 

The main script is Tree_Stat.R
All input data for this script are available on Dryad
(see DOI in the Data Availability section of the paper).


